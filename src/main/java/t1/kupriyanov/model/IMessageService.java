package t1.kupriyanov.model;

import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/v2")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient()
@ClientHeaderParam(name = "Content-Type", value = "application/json")
@ClientHeaderParam(name = "Authorization", value = "Bearer t1.9euelZqZmMrImY2WjpydlZOZlYmTk-3rnpWams_NzpaNnZKZyJKSjcePlsjl9PcGEFln-e8dNmnm3fT3Rj5WZ_nvHTZp5g.zFMUBdjFSbgvhQnVse6HhK7pDR3kKwaOqhkymDbROZ3QihXqkkmsT3DecTo7YrzzdmgV7P_6ygPcO9m7Is8DCQ")
public interface IMessageService {

    @POST
    @Path("/translate")
    @Produces(MediaType.APPLICATION_JSON)
    Response callTranslator(Message message);

}
