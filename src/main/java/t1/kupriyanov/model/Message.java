package t1.kupriyanov.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.quarkus.hibernate.reactive.panache.PanacheEntity;
import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import java.net.DatagramSocket;
import java.net.InetAddress;

@Entity
@Cacheable
public class Message extends PanacheEntity {

    //before translation
    private String texts;

    //after translation
    private String textsAfter;

    private String detectedLanguageCode;

    private String targetLanguageCode;

    private String ipAddress;

    private final String folderId = "b1gbq53oilqej17rl68v";

    public Message() {
    }

    public Message(Long id, String texts, String textsAfter, String detectedLanguageCode, String targetLanguageCode, String ipAddress) {
        this.id = id;
        this.texts = texts;
        this.textsAfter = textsAfter;
        this.detectedLanguageCode = detectedLanguageCode;
        this.targetLanguageCode = targetLanguageCode;
        this.ipAddress = ipAddress;
    }

    public String getIpAddress(){
        try (final DatagramSocket socket = new DatagramSocket()) {
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            return socket.getLocalAddress().getHostAddress();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "127.0.0.1";
    }

    public static Multi<Message> findAll(PgPool client) {
        return client.query("SELECT * FROM messages_table ORDER BY id ASC").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(Message::from);
    }

    private static Message from(Row row) {
        return new Message(
                row.getLong("id"),
                row.getString("texts"),
                row.getString("textsafter"),
                row.getString("detectedlanguagecode"),
                row.getString("targetlanguagecode"),
                row.getString("ipaddress")
        );
    }

    public String getTexts() {
        return texts;
    }

    public void setTexts(String strBefore) {
        this.texts = strBefore;
    }

    public String getTextsAfter() {
        return textsAfter;
    }

    public void setTextsAfter(String textsAfter) {
        this.textsAfter = textsAfter;
    }

    public String getTargetLanguageCode() {
        return targetLanguageCode;
    }

    public void setTargetLanguageCode(String langAfter) {
        this.targetLanguageCode = langAfter;
    }

    public String getDetectedLanguageCode() {
        return detectedLanguageCode;
    }

    public void setDetectedLanguageCode(String detectedLanguageCode) {
        this.detectedLanguageCode = detectedLanguageCode;
    }

    public String getFolderId() {
        return folderId;
    }

    @Override
    public String toString() {
        return "Message{" +
                "texts='" + texts + '\'' +
                ", textsAfter='" + textsAfter + '\'' +
                ", detectedLanguageCode='" + detectedLanguageCode + '\'' +
                ", targetLanguageCode='" + targetLanguageCode + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", folderId='" + folderId + '\'' +
                '}';
    }

}
