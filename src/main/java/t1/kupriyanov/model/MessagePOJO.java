package t1.kupriyanov.model;

public class MessagePOJO {

    public String text;

    public String detectedLanguageCode;

    public String getText() {
        return text;
    }

    public String getDetectedLanguageCode() {
        return detectedLanguageCode;
    }

}
