package t1.kupriyanov.resource;

import io.quarkus.runtime.StartupEvent;
import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Tuple;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import t1.kupriyanov.model.IMessageService;
import t1.kupriyanov.model.Message;
import t1.kupriyanov.model.MessagePOJOList;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/translator")
@ApplicationScoped
public class MessageResource {

    @Inject
    PgPool client;

    @Inject
    @RestClient
    IMessageService messageService;

    @Inject
    @ConfigProperty(name = "myapp.schema.create", defaultValue = "true")
    boolean schemaCreate;

    void config(@Observes StartupEvent ev) {
        if (schemaCreate) {
            initdb();
        }
    }

    private void initdb() {
        client.query("DROP TABLE IF EXISTS messages_table").execute()
                .flatMap(r -> client.query("CREATE TABLE messages_table (" +
                        "id SERIAL PRIMARY KEY, " +
                        "texts TEXT NOT NULL," +
                        "textsAfter TEXT," +
                        "detectedLanguageCode TEXT NOT NULL," +
                        "targetLanguageCode TEXT," +
                        "ipAddress TEXT," +
                        "messageTime TIMESTAMP)").execute())
                .flatMap(r -> client.query("INSERT INTO messages_table (texts, detectedlanguagecode) " +
                        "VALUES ('Hello World', 'en')").execute())
                .await().indefinitely();
    }

    private void writedb(Response response, Message message) {
        MessagePOJOList pojo = response.readEntity(MessagePOJOList.class);
        client.preparedQuery("INSERT INTO messages_table (" +
                "texts, " +
                "textsafter, " +
                "detectedlanguagecode, " +
                "targetlanguagecode, " +
                "ipaddress," +
                "messageTime" +
//                ") VALUES ('10', 'texts', 'textsafter', 'detectedlanguage', 'targetlanguage', 'ipaddress')").execute().await().indefinitely();
                ") VALUES ($1, $2, $3, $4, $5, NOW())").execute(Tuple.of(
                        message.getTexts(),
                        pojo.translations.get(0).getText(),
                        pojo.translations.get(0).getDetectedLanguageCode(),
                        message.getTargetLanguageCode(),
                        message.getIpAddress())
                ).await().indefinitely();
    }

    @Path("/get")
    @Operation(summary = "Перевод")
    @APIResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON, schema = @Schema(implementation = Message.class)))
    @APIResponse(responseCode = "204", description = "Bad input")
    @POST
    public void get(@QueryParam("texts") String texts, @QueryParam("targetLanguageCode") String targetLanguageCode) {
        Message message = new Message();
        message.setTexts(texts);
        message.setTargetLanguageCode(targetLanguageCode);
        Response response = messageService.callTranslator(message);
//        response.bufferEntity();
        writedb(response, message);
//        return response;
    }

    @Path("/getall")
    @Operation(summary = "Вывод всех записей из базы данных")
    @GET
    public Multi<Message> getAll() {
        return Message.findAll(client);
    }

}
